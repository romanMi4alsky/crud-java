package ru.noorsoft.javaeducation.dao;

import org.springframework.data.repository.CrudRepository;
import ru.noorsoft.javaeducation.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}