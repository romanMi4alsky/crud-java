package ru.noorsoft.javaeducation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.noorsoft.javaeducation.entity.User;
import ru.noorsoft.javaeducation.service.UserService;

@Controller
public class UsersController {
    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String showUsersList(Model model) {
        model.addAttribute("users",userService.findAll());
        model.addAttribute("user",new User());
        return "users_list";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("user") User user){
        System.out.println(user);
        userService.save(user);

        return "redirect:/";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("userId") Long userId){
        userService.delete(userId);

        return "redirect:/";
    }
}