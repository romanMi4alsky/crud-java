package ru.noorsoft.javaeducation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.noorsoft.javaeducation.dao.UserRepository;
import ru.noorsoft.javaeducation.entity.User;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User findById(Long userId) {
        Optional<User> result = userRepository.findById(userId);

        User user = null;
        if(result.isPresent()){
            user = result.get();
            return user;
        }
        else {
            throw new RuntimeException("Did not found user id - " + userId);
        }
    }

    @Override
    public void delete(Long userId) {
        User user = findById(userId);

        userRepository.delete(user);
    }
}
