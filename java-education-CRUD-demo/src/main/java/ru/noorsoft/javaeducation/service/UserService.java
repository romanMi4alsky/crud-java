package ru.noorsoft.javaeducation.service;

import ru.noorsoft.javaeducation.entity.User;

import java.util.List;

public interface UserService {

    public List<User> findAll();

    void save(User user);

    User findById(Long userId);

    void delete(Long userId);
}
